use clap::Parser;

#[derive(Parser, Debug, Clone)]
#[clap(
    author = "re-ŠOL",
    version,
    about,
    long_about = "re-ŠOL CLI tool"
)]
pub struct Args {
    // Actions
    //
    /// Information about authorization
    #[clap(long, short, value_parser, default_value_t = false)]
    pub info: bool,

    /// Information about user 
    #[clap(long, value_parser, default_value_t = false)]
    pub user_info: bool,

    /// Pass username
    #[clap(long, short, value_parser, default_value_t = String::from("user"))]
    pub user: String,

    /// Pass password
    #[clap(long, short, value_parser, default_value_t = String::from("password"))]
    pub pass: String,

    /// Set ŠOL url (ex: https://aplikace.skola-online.cz) 
    #[clap(long, value_parser, default_value_t = String::from("https://aplikace.skolaonline.cz"))]
    pub url: String
}

use reqwest::header::{REFERER, USER_AGENT};
use crate::args::Args;
use crate::cli::requests::*;

use std::io::Write;

pub fn try_user_info(
    args: &Args,
    client: reqwest::blocking::Client,
    user_agent: &String,
    referer: &String
) -> Result<std::string::String, reqwest::Error> {
    // Get a status request
    l_start_request();
    std::io::stdout().flush().unwrap();
    let data = client
        .get(format!("{}/SOLWebApi/api/v1/UzivatelInfo/{}", args.url, args.user))
        .basic_auth(&args.user, Some(&args.pass))
        .header(USER_AGENT, user_agent)
        .header(REFERER, referer)
        .send()
        .expect("Failed to get a request")
        .text();
    l_finish_request();
    data
}


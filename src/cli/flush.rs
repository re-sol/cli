use std::io::Write;

pub fn flush_output() {
    std::io::stdout().flush().unwrap();
}

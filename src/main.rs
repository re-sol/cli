use std::process::exit;

use clap::Parser;
use reqwest::header::{self, ACCEPT_CHARSET, CACHE_CONTROL, CONNECTION};
use rand::Rng;

pub mod args;
mod user_agent;
mod referers;
mod core;
pub mod cli;

use crate::user_agent::user_agent_list;
use crate::referers::referers_list;
use crate::core::authorization::try_auth;
use crate::core::user_info::try_user_info;

use args::Args;

fn main() {
    // Parse arguments
    let args = Args::parse();

    let mut rng = rand::thread_rng();

    let tor_proxy = format!("socks5h://127.0.0.1:9050");

    // Create header map
    let mut default_headers = header::HeaderMap::new();

    // Insert headers into header map
    default_headers.insert(CONNECTION, header::HeaderValue::from_static("keep-alive"));
    default_headers.insert(CACHE_CONTROL, header::HeaderValue::from_static("no-cache"));
    default_headers.insert(
        ACCEPT_CHARSET,
        header::HeaderValue::from_static("ISO-8859-1,utf-8;q=0.7,*;q=0.7"),
    );
    
    let client = reqwest::blocking::Client::builder()
        .proxy(reqwest::Proxy::all(tor_proxy).unwrap())
        .default_headers(default_headers)
        .build()
        .expect("Failed to build the client");

    // User agent randomization
    let user_agent_list = user_agent_list();
    let random_user_agent_index = rng.gen_range(0..user_agent_list.len());

    // Referers randomization
    let referers_list = referers_list();
    let random_referer_index = rng.gen_range(0..referers_list.len());



    // COMMANDS
    if args.info {
        let data = try_auth(
            &args,
            client,
            &user_agent_list[random_user_agent_index],
            &referers_list[random_referer_index]
        );
        // Print debug information
        // println!("{}", data.as_ref().unwrap()); // Print JSON
        // println!("User agent: {}", user_agent_list[random_user_agent_index]); // Print user agent
        
        // Parse JSON
        let json: serde_json::Value =
            serde_json::from_str(&data.unwrap()).expect("JSON is not formated well");
        
        // Get Data from JSON
        let authenticated: bool = serde_json::from_value(
            json.get("Data")
                .expect("Could not read Data key from JSON")
                .to_owned(),
        ).unwrap();

        let mut message: String = json["Status"]["Message"].to_string().replace('"', "");
        let code: String = json["Status"]["Code"].to_string().replace('"', "");
        let details: String = json["Status"]["Details"].to_string().replace('"', "");

        if message.is_empty() {
            message = String::from("None");
        }

        println!("Autheticated: {authenticated}");
        println!("Message: {message}");
        println!("Code: {code}");
        println!("Details: {details}");

        if authenticated {
            exit(0);
        } else {
            exit(1);
        }
    }

    if args.user_info {
        let data = try_user_info(
            &args,
            client,
            &user_agent_list[random_user_agent_index],
            &referers_list[random_referer_index]
        );
        // Print debug information
        // println!("{}", data.as_ref().unwrap()); // Print JSON
        
        // Parse JSON
        let json: serde_json::Value =
            serde_json::from_str(&data.unwrap()).expect("JSON is not formated well");
        
        let trida: String = json["Data"]["TRIDA_NAZEV"].to_string().replace('"', "");
        let uziv_jmeno: String = json["Data"]["UZIV_JMENO"].to_string().replace('"', "");
        let uziv_id: String = json["Data"]["UZIVATEL_ID"].to_string().replace('"', "");
        let osoba_id: String = json["Data"]["OSOBA_ID"].to_string().replace('"', "");
        let trida_id: String = json["Data"]["TRIDA_ID"].to_string().replace('"', "");
        let jmeno: String = json["Data"]["JMENO"].to_string().replace('"', "");
        let kategorie: String = json["Data"]["KATEGORIE_ID_CSV"].to_string().replace('"', "");

        println!("Trida:\t\t{trida}");
        println!("Uziv. Jmeno:\t{uziv_jmeno}");
        println!("Uziv. ID:\t{uziv_id}");
        println!("Osoba ID:\t{osoba_id}");
        println!("Trida ID:\t{trida_id}");
        println!("Jmeno:\t\t{jmeno}");
        println!("Kategorie:\t{kategorie}");
        exit(0);
    } 

    println!("Try `--help` argument for usage");
    exit(1);
}
    

